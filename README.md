# Nature Of Code 03 #

Custom distribution

Inspirated by Daniel Shiffman
(http://natureofcode.com/)

### How do I get set up? ###

#### On OSX: ####

brew install sdl2

https://github.com/gosu/gosu/wiki/Getting-Started-on-OS-X

#### ON LINUX ####

You need the following packages: libsdl2-dev, libsdl2-ttf-dev, libpango1.0-dev, libgl1-mesa-dev, libopenal-dev, libsndfile-dev, libmpg123-dev

https://github.com/gosu/gosu/wiki/Getting-Started-on-Linux

#### ON WINDOWS ####

You don't have to do anything

https://github.com/gosu/gosu/wiki/Getting-Started-on-Windows

#### ON RPI ####

Following this steps:

https://github.com/gosu/gosu/wiki/Getting-Started-on-Raspbian-%28Raspberry-Pi%29

#### After these steps ####

$ bundle

### Screenshot: ###
![custom-distribution.png](https://bitbucket.org/repo/akkdnan/images/1390168883-custom-distribution.png)