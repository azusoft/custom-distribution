#Montecarlo

class Montecarlo
  attr_accessor :value
  def initialize(min = 0, max = 100)
    @found = false
    @max_try = 10000
    @trials = 0
    while !@found && @trials < @max_try do
      @randome_one = rand(0..max)
      @randome_two = rand(0..max)
      @result = @randome_one * @randome_two
      if @randome_two < @result
        @found = true
        @value = @randome_one
        return @value
      end
      ++@trials
    end
    @value = 0
    return @value
  end
end