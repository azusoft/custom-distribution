#Basic file

require 'gosu'
load 'range.rb'
load 'montecarlo.rb'

WINDOW_HEIGHT = 320
WINDOW_WIDTH = 568

class Window < Gosu::Window
  def initialize
    super WINDOW_WIDTH, WINDOW_HEIGHT, false, 1
    self.caption = "Custom Distribution"
    @heights = Array.new(WINDOW_WIDTH, 0)
    puts @heights.length
  end

  def update
    @montecarlo = Montecarlo.new(0, WINDOW_WIDTH)
    @index = (0..@heights.length - 1).bounding(@montecarlo.value.to_i)
    @heights[@index] = (0..WINDOW_HEIGHT).bounding(@heights[@index] + 1)
  end

  def draw
    @heights.each_with_index do |height, index|
      Gosu.draw_rect(index - 1, WINDOW_HEIGHT - height, 1, height, Gosu::Color.new(0xffffffff))
    end
  end
end

window = Window.new
window.show